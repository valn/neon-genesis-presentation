# NGE Presentation 🤖

<div align="center">
  <img src="https://pm1.aminoapps.com/7633/f7d7ec1ef31aae1a8c6c50e7846ef62758caec51r1-753-564v2_hq.jpg" width="40%" height="40%" alt="EVA Unit-01">
</div>

## Instrucciones 👾

1. Cloná este repositorio en un termnal utilizando el comando:

    ```bash
    git clone https://gitlab.com/valn/neon-genesis-presentation.git
    ```

2. Abrí el archivo index.html con tu editor de texto preferido:

    ```bash
    code ./index.html
    ```

3. ¡Armá tus diapositivas y preparate para presentar!

## Tecnologías 🛠️

-   HTML5
-   CSS3
-   JavaScript

## ¿Contribuidores? 🧑‍🤝‍🧑

-   Shinji Ikari 😣
-   Rei Ayanami 😐
-   Asuka Langley Sōryū 😒
-   Misato Katsuragi 🍺

## Capturas de la plantilla 📷

<div align="center">
  <img src="https://i.imgur.com/mtQeUt4.png">
  <img src="https://i.imgur.com/RYFjKqk.png">
  <img src="https://i.imgur.com/3O4SaCR.png">
  <img src="https://i.imgur.com/3o7DI5S.png">
</div>

## Capturas de un ejemplo 📺

<div align="center">
  <img src="https://i.imgur.com/MVFGgxu.png">
  <img src="https://i.imgur.com/A7gOy43.png">
  <img src="https://i.imgur.com/nDQiOiU.png">
  <img src="https://i.imgur.com/UcLQ1QJ.png">
  <img src="https://i.imgur.com/q3i2pec.png">
  <img src="https://i.imgur.com/K466uit.png">
  <img src="https://i.imgur.com/Q0MWL4Y.png">
</div>

## Licencia 📜

¡Este proyecto se distribuye bajo la licencia de NERV!  
Quiero decir, [MIT](https://choosealicense.com/licenses/mit/).

Como sea..., ¡nos vemos en el tercer impacto! 🌌

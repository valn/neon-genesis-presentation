const dots = document.querySelectorAll('.page-indicator .dot');

function updateDots() {
    dots.forEach((dot, index) => {
        if (index === currentPage - 1) {
            dot.classList.add('active');
        } else {
            dot.classList.remove('active');
        }
    });
}

updateDots();

document.addEventListener('keydown', (event) => {
    if (event.key === 'ArrowLeft' && currentPage > 1) {
        currentPage--;
    } else if (event.key === 'ArrowRight' && currentPage < totalPages) {
        currentPage++;
    }
    updatePage();
    updateDots();
});

function updatePage() {
    slides.forEach((slide, index) => {
        if (index + 1 === currentPage) {
            slide.classList.add('active');
        } else {
            slide.classList.remove('active');
        }
    });
}
